#!/bin/bash

#初始化mysql,并获取初始化密码 并启动mysql
mysqld --defaults-file=/home/mysql/mysql-5.7/my.cnf --user=mysql --basedir=/home/mysql/mysql-5.7/ --datadir=/home/mysql/mydata/  --initialize


init_pass=`cat /home/mysql/mysql_logs/mysql.log |grep root@localhost: |awk '{print $11}'`
echo 初始化默认密码: ${init_pass}
cat >/home/mysql/init_password <<EOF
${init_pass}
EOF

/bin/sh /home/mysql/scripts/start.sh
