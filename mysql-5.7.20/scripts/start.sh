#!/bin/bash
my_cnf=/home/mysql/mysql-5.7/my.cnf
mysql_bin=/home/mysql/mysql-5.7/bin

User='mysql'

if [ `whoami`  !=  $User ];then
    echo 'please use $User user'
    exit
fi


ps -ef |grep mysqld |grep -v grep
if [ $? == 0 ];then
    echo  "MySQL is running..."
    exit
else
    echo -e  "\033[32mMySQL is starting. please wait...\033[0m"
    cd $mysql_bin
    $mysql_bin/mysqld --defaults-file=$my_cnf &
fi
