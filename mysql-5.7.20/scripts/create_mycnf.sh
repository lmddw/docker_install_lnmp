#!/bin/bash
cat > /home/mysql/mysql-5.7/my.cnf <<EOF
[mysqld]
basedir=/home/mysql/mysql-5.7/
datadir=/home/mysql/mydata/
character_set_server=utf8
init_connect='SET NAMES utf8'
port=3366
lower_case_table_names=1
max_connections=1000
sql_mode=STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
skip-name-resolve
log-error=/home/mysql/mysql_logs/mysql.log

[clinet]
default-character-set=utf8
EOF
